@extends('main')
@section('content')

<div class="container">
    <div class="row mt-5">
        @if($products['status']==200)
            @if(count($products['data'])>0)
                @foreach($products['data'] as $product)
                <div class="col-md-4 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->title }}</h5>
                            <p class="card-text">{{ $product->body_html }}</p>
                            <a href="{{url(\URL::current().'/'.$product->id.'/'.$product->title)}}" class="btn btn-primary">{{$product->handle}}</a>
                        </div>
                    </div>
                </div>
                @endforeach                
            @else 
                <p class="card-text">Invalid request</p>
            @endif    
        @else 
            <p class="text-danger">{{$products['data']}}</p>        
        @endif
    </div>
</div>

@stop
