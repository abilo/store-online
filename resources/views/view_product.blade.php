@extends('main')
@section('content')

<div class="container">
    <div class="row">
            <div class="col-md-10 pt-5">
                <div class="card">
                    <div class="card-body">
                        @if($product['status']==200)
                            <h5 class="card-title">{{$product['data'][0]->title}}</h5>
                            <p class="card-text">{{ $product['data'][0]->body_html }}</p>
                            <a href="#" class="btn btn-primary">{{$product['data'][0]->handle}}</a>
                        @else 
                            <p class="text-danger">{{$product['data']}}</p>
                        @endif
                    </div>
                </div>
            </div>
    </div>
</div>

@stop
