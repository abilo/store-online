<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Link;
use App\Models\Authorisation;
use Illuminate\Support\Facades\Http;

class ProductController extends Controller
{
    public function index(Request $request, $store_id)
    {
        $authorisation = Authorisation::where('store_id',$store_id)->first();        
        if($authorisation) $api_key = $authorisation->api_key;
        else return response(['status'=>'401', 'data'=>'Unauthorized']);

        $link = Link::where('store_id',$store_id)->first();
        if($link) $link_id = $link->link;
        else return response('Unauthorized.', 401);        

        $url = $link_id.'?api_access='.$api_key;
        $response = Http::get($url);

        if($response->status()==200) $products = ['status'=>$response->status(), 'data'=>json_decode($response->getbody()) ]; 
        else $products = ['status'=>$response->status(), 'data'=>$response->reason() ];

//        dump(json_decode($response->getbody()));
//        collect($response->json())->toJson();

        return view('list_products', compact('products','link'));
    }

    public function view_product(Request $request, $store_id, $id)
    {
        $authorisation = Authorisation::where('store_id',$store_id)->first();        
        if($authorisation) $api_key = $authorisation->api_key;
        else return response(['status'=>'Unauthorized', 'data'=>'401']);

        $link = Link::where('store_id',$store_id)->where('action','view_product')->first();
        if($link) $link_id = $link->link;
        else return response('Unauthorized.', 401);        

        $url = $link_id.'/'.$id.'?api_access='.$api_key;
        $response = Http::get($url);

        if($response->status()==200) $product = ['status'=>$response->status(), 'data'=>json_decode($response->getbody()) ]; // $product = json_decode($response->getbody()); 
        else $product = ['status'=>$response->status(), 'data'=>$response->reason() ];

//        dump($response->status());
//        dump(json_decode($response->getbody()));

        return view('view_product', compact('product'));

    }

}
