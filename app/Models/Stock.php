<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory;
    protected $table = 'stocks';

    protected $fillable = [
        'qte',
        'in_qte',
        'out_qte',
        'adjust_qte',
        'current',
        'variant_id'
    ];

// Relationship
    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }

}
