<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends Model
{
    use HasFactory;
    protected $table = 'links';

    protected $fillable = [
        'title',
        'link',
        'method',
        'status',
        'store_id'
    ];

// Relationship
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function authorisations()
    {
        return $this->belongsToMany(Authorisation::class, 'authorisations_links')->withPivot('start_date','expiry_date','number_request');
    }

}
