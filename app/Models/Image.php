<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use HasFactory;
    protected $table = 'images';

    protected $fillable = [
        'imagetable_id',
        'imagetable_type',
        'name',
        'size',
        'format'
    ];

// Relationship
    public function imagetable()
    {
        return $this->morphTo();
    }

}
