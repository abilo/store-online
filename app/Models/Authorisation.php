<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Authorisation extends Model
{
    use HasFactory;

    protected $table = 'authorisations';
    protected $fillable = [
        'api_key',
        'status',
        'store_id'
    ];

// Relationship
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function links()
    {
        return $this->belongsToMany(Link::class, 'authorisations_links')->withPivot('start_date','expiry_date','number_request');
    }


}
