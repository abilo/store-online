<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model
{
    use HasFactory;
    protected $table = 'variants';

    protected $fillable = [
        'sku',
        'price',
        'taxable',
        'weight',
        'taxable',
        'requires_shipping',
        'size',
        'status',
        'price_promotion',
        'product_id'
    ];

// Relationship
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imagetable');
    }

}
