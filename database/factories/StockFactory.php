<?php

namespace Database\Factories;

use App\Models\Stock;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockFactory extends Factory
{
    protected $model = Stock::class;

    public function definition(): array
    {
    	return [
            'qte' => $this->faker->numberBetween(1,100),
            'in_qte' =>$this->faker->numberBetween(1,100),
            'out_qte' => $this->faker->numberBetween(1,100),
            'adjust_qte' =>$this->faker->numberBetween(1,100),
            'current' =>$this->faker->boolean,
            'variant_id' =>$this->faker->numberBetween(1,10)
    	];
    }
}
