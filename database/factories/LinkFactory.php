<?php

namespace Database\Factories;

use App\Models\Link;
use Illuminate\Database\Eloquent\Factories\Factory;

class LinkFactory extends Factory
{
    protected $model = Link::class;

    public function definition(): array
    {
    	return [
            'action' => 'view_product',
            'link' => 'http://localhost:8080/api/products',
            'method' => 'GET',
            'status' =>'actived',
            'store_id' => 1
    	];
    }
}
