<?php

namespace Database\Factories;

use App\Models\Authorisation;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorisationFactory extends Factory
{
    protected $model = Authorisation::class;

    public function definition(): array
    {
    	return [
            'api_key' => '000000000',
            'status' => 'actived',
            'store_id' => 1
    	];
    }
}
