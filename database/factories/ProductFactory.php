<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->word,
            'body_html' => $this->faker->text(150),
            'handle' => $this->faker->word,
            'product_type' => $this->faker->numberBetween(1,10),
            'store_id' => $this->faker->numberBetween(1,10)
        ];
    }
}
