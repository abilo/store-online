<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LinksTableSeeder::class,
            AuthorisationsTableSeeder::class
//            UsersTableSeeder::class,
//            ProductsTableSeeder::class,
//            ImagesTableSeeder::class,
//            StocksTableSeeder::class,
//            VariantsTableSeeder::class
        ]);
    }
}
