<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Authorisation;

class AuthorisationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Authorisation::factory(1)->create();
    }
}
