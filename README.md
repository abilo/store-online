<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Download and configuration

- Download the store-Online on your machine.
- Create a database on your machine with the name db_store as mentioned in the .env file or change the name as you wish in the .env file (DB_DATABASE=db_storeonline).
- Open the command line CMD and go to the root of the project folder.
- To launch store-Online type this command: "php artisan serve".
- Type this command: "php artisan migrate" to migrate and create tables in your database.
- Type this command: "php artisan db:seed" to fill the table table and the Link table in order to automatically test your store-API. I assume we have this data in the Authorizations table and the Links table.
- Change the value of the api_key field in the Links table with api_access that we receive from store-Api after registration. If you have another URL or Port that uses your store-Api, you can change the value of the link field in the Links table. 
The Authorisation table contains concerted data on security access, and the Links table contains the links of any operation that will be passed on to the Retailer server.  

## Test API by Swagger

- To check if your store-Online is working or not, type in your browser http://localhost:8000.
- Now to see the list of products of retailer/store 1, type this URL in your browser: http://localhost:8000/products/store/1  we can structure or add any parameter in the URL to comply with your current URL and SEO strategy, it is just a simple way or URL to access and get data, the last parameter 1 in the URL means the shop id in our database.
- In order to see the details of a product, just click on the blue button in any product on the page.

## Security

Note: I created store-Online to give you an easy way to test store-API, you can even type any number instead of a real shop or product ID to see the interface response and error handling.

## Model and Logic

To see the store-online model, go to ..store-online and choose any model to see the model and the relationship between the models.
To see the logic of store-online and how to run the store API, go to ..\store-onlineapp\Http\Controllers after clicking on ProductController.php.
